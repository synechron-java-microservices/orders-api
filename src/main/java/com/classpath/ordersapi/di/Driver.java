package com.classpath.ordersapi.di;

public class Driver {

    public void commute(String boardingLoc, String destination){
        System.out.println("Travelling from "+boardingLoc + " to "+ destination);
    }
}