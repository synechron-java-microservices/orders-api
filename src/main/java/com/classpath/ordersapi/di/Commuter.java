package com.classpath.ordersapi.di;

import org.springframework.stereotype.Component;

@Component
public class Commuter {

    private Driver driver;

    public Commuter(Driver driver){
        this.driver = driver;
    }

    public  void travel(String location, String destination){
        this.driver.commute(location, destination);
    }
}