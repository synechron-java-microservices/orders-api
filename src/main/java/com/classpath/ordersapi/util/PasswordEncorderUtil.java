package com.classpath.ordersapi.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Slf4j
public class PasswordEncorderUtil {

    public static void main(String[] args) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        final String encodedPassword1 = passwordEncoder.encode("welcome");
        final String encodedPassword2 = passwordEncoder.encode("welcome");
        final String encodedPassword3 = passwordEncoder.encode("welcome");
        final String encodedPassword4 = passwordEncoder.encode("welcome");

        log.info("Encoded password:: {}", encodedPassword1);
        log.info("Encoded password:: {}", encodedPassword2);
        log.info("Encoded password:: {}", encodedPassword3);
        log.info("Encoded password:: {}", encodedPassword4);

        log.info("Password matched:: {}",passwordEncoder.matches("welcome", encodedPassword1));
        log.info("Password matched:: {}",passwordEncoder.matches("welcome", encodedPassword2));
        log.info("Password matched:: {}",passwordEncoder.matches("welcome", encodedPassword3));
        log.info("Password matched:: {}",passwordEncoder.matches("welcome", encodedPassword4));
    }
}