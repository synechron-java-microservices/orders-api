package com.classpath.ordersapi.controller;

import com.classpath.ordersapi.model.Customer;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/customers")
@RequiredArgsConstructor
@Slf4j
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping
    @Operation(summary = "Fetching the customers")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Fetching the customers",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Customer.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Customer with given id not found",
                    content = @Content) })
    public Set<Customer> fetchCustomers(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info(" Came inside the fetch customers method:: {}", ((UserDetails)authentication.getPrincipal()).getUsername() );
        return this.customerService.fetchCustomers();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Fetching the customers by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Fetching the customer by id",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Customer.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Customer with given id not found",
                    content = @Content) })
    public Customer fetchCustomerById(@PathVariable @Parameter(name = "id", description = "customer id to be fetched") long id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info(" Came inside the fetch customers method:: {}", authentication.getPrincipal() );
        return this.customerService.fetchCustomerById(id);
    }

    @GetMapping("/{id}/orders")
    public Set<Order> fetchOrdersByCustomerId(@PathVariable long id){
        return this.customerService.fetchOrdersByCustomerId(id);
    }

    @PostMapping
    public Customer placeOrder(@RequestBody Customer customer){
        Set<Order> orders = customer.getOrders();
        orders.forEach(order -> order.getOrderLineItems().forEach(orderLineItem -> orderLineItem.setOrder(order)));
        customer.getOrders().forEach(order -> order.setCustomer(customer));
        return  this.customerService.save(customer);
    }

}