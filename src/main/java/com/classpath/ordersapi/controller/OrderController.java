package com.classpath.ordersapi.controller;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderController {

    private final OrderService orderService;

    @GetMapping
    public Set<Order> fetchOrders(){
        log.debug("Came inside the fetch Orders method :: ");
        return this.orderService.fetchOrders();
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable long id){
        log.debug("Came inside the fetch Orders method :: ");
        return this.orderService.findById(id);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Order saveOrder(@RequestBody @Valid Order order){
        log.debug("Came inside the saveOrder method :: {} ", order);
        return this.orderService.save(order);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteOrder(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }
}