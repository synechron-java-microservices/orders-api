package com.classpath.ordersapi.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Configuration
@RequiredArgsConstructor
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        /*authenticationManagerBuilder
                .inMemoryAuthentication()
                .withUser("kiran")
                .password("{noop}welcome")
                .roles("CUSTOMER")
                .and()
                .withUser("vinay")
                .password("{noop}welcome")
                .roles("CUSTOMER", "ADMIN");*/
        authenticationManagerBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(this.passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors().disable();
        http.headers().frameOptions().disable();

        http
                .authorizeRequests()
                .antMatchers("/login", "/h2-console/**", "/swagger-ui/index.html")
                .permitAll()
                .antMatchers(GET, "/api/v1/customers/**")
                .hasRole("CUSTOMER")
                .antMatchers(POST, "/api/v1/customers/**")
                .hasRole("MANAGER")
                .antMatchers(HttpMethod.DELETE, "/api/v1/customers")
                .hasRole("ADMIN")
                .anyRequest()
                .fullyAuthenticated()
                .and()
                .formLogin()
                .and()
                .httpBasic();
    }
}