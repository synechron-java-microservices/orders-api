package com.classpath.ordersapi.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class ApplicationConfiguration { //implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public ApplicationConfiguration (ApplicationContext applicationContext){
        this.applicationContext = applicationContext;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /*@Override
    public void run(String... args) throws Exception {
        System.out.println("== Hello World Spring Boot with dependency injection ====");
        String[] beanDefinitionNames = this.applicationContext.getBeanDefinitionNames();
        for(String beanName: beanDefinitionNames){
            if ( beanName.equalsIgnoreCase("driver") || beanName.equalsIgnoreCase("uberCustomer")) {
                System.out.println("Bean Name :: " + beanName);
            }
        }
        Customer customer = this.applicationContext.getBean("customer", Customer.class);
        if (customer != null) {
            customer.travel("Bangalore", "Mumbai");
        }
    }*/
}