package com.classpath.ordersapi.config;

import com.classpath.ordersapi.di.Driver;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AutoConfiguration {

    //Java style of creating a Spring Bean
    @Bean
    @ConditionalOnProperty(name="app.company", havingValue = "uber" , matchIfMissing = true)
    public Driver driver(){
        return new Driver();
    }

    @Bean
    @ConditionalOnMissingBean(name="driver")
    public Driver olaDriver(){
        return new Driver();
    }

    /*@Bean
    public Customer uberCustomer(){
        return new Customer(olaDriver());
    }*/
}