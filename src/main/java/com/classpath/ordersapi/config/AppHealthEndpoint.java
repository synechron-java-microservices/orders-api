package com.classpath.ordersapi.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import static org.springframework.boot.actuate.health.Status.UP;

@Component
class PaymentServiceEndpoint implements HealthIndicator {

    @Override
    public Health health() {
        //try calling the payment service
        return Health.status(UP).withDetail("PaymentService", "Payment service is UP").build();
    }
}

@Component
class KafkaHealthEndpoint implements HealthIndicator{

    @Override
    public Health health() {
        //consume a message from the topic
        //kafkaconstumer.subscribe to topic or post a message to the topic
        return Health.up().withDetail("Kafka-Service", "Kafka broker is up").build();
    }

}