package com.classpath.ordersapi.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
@Component
public class AppExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleInvalidOrder(Throwable throwable){
        return ResponseEntity.status(BAD_REQUEST).body(Error.builder().code(100).message(throwable.getMessage()).build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Error> handleRuntimeError(Throwable throwable){
        return ResponseEntity.status(BAD_REQUEST).body(Error.builder().code(200).message(throwable.getMessage()).build());
    }
}

@AllArgsConstructor
@Builder
@Getter
@NoArgsConstructor
class Error {
    private int code;
    private String message;
}