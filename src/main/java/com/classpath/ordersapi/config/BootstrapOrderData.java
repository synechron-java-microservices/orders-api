package com.classpath.ordersapi.config;

import com.classpath.ordersapi.model.*;
import com.classpath.ordersapi.repository.CustomerRepository;
import com.classpath.ordersapi.repository.RoleRepository;
import com.classpath.ordersapi.repository.UserRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import java.time.LocalDate;

@Component
@RequiredArgsConstructor
@Slf4j
public class BootstrapOrderData implements ApplicationListener<ApplicationReadyEvent> {

    //private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;


    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {/*
        Order order1 = Order.builder().customerName("Suresh").orderDate(LocalDate.now()).price(45_000).build();
        Order order2 = Order.builder().customerName("Mahesh").orderDate(LocalDate.now()).price(50_000).build();
        Order order3 = Order.builder().customerName("Vikas").orderDate(LocalDate.now()).price(75_000).build();
        Order order4 = Order.builder().customerName("Sourabh").orderDate(LocalDate.now()).price(35_000).build();
        this.orderRepository.save(order1);
        this.orderRepository.save(order2);
        this.orderRepository.save(order3);
        this.orderRepository.save(order4);*/
        Faker faker = new Faker();

        Customer customer1 = Customer.builder()
                .dob(LocalDate.of(faker.number().numberBetween(1960, 1990), faker.number().numberBetween(2, 5), faker.number().numberBetween(2, 28)))
                .name(faker.name().fullName())
                .build();

        Customer customer2 = Customer.builder()
                .dob(LocalDate.of(faker.number().numberBetween(1960, 1990), faker.number().numberBetween(2, 5), faker.number().numberBetween(2, 28)))
                .name(faker.name().fullName())
                .build();

        Order order1 = Order.builder().orderDate(LocalDate.now()).price(35000).build();
        Order order2 = Order.builder().orderDate(LocalDate.now()).price(45000).build();

        OrderLineItem lineItem1 = OrderLineItem.builder().pricePerUnit(3500).qty(10).build();
        OrderLineItem lineItem2 = OrderLineItem.builder().pricePerUnit(4500).qty(8).build();
        OrderLineItem lineItem3 = OrderLineItem.builder().pricePerUnit(8000).qty(6).build();
        OrderLineItem lineItem4 = OrderLineItem.builder().pricePerUnit(25000).qty(2).build();

        order1.addLineItem(lineItem1);
        order1.addLineItem(lineItem2);

        order2.addLineItem(lineItem3);
        order2.addLineItem(lineItem4);

        customer1.addOrder(order1);

        customer2.addOrder(order2);

        this.customerRepository.save(customer1);
        this.customerRepository.save(customer2);

        //users and roles and save it in the database
        User kiran = User.builder().password(this.passwordEncoder.encode("welcome")).username("kiran").emailAddress("kiran@gmail.com").build();
        User vinay = User.builder().password(this.passwordEncoder.encode("welcome")).username("vinay").emailAddress("vinay@gmail.com").build();

        Role userRole = Role.builder().roleName("USER").build();
        Role adminRole = Role.builder().roleName("ADMIN").build();
        Role customerRole = Role.builder().roleName("CUSTOMER").build();
        Role managerRole = Role.builder().roleName("MANAGER").build();

        kiran.addRole(userRole);
        kiran.addRole(customerRole);

        vinay.addRole(userRole);
        vinay.addRole(customerRole);
        vinay.addRole(adminRole);
        vinay.addRole(managerRole);

        this.userRepository.save(kiran);
        this.userRepository.save(vinay);

        this.roleRepository.save(userRole);
        this.roleRepository.save(customerRole);
        this.roleRepository.save(adminRole);
        this.roleRepository.save(managerRole);

        log.info("-------------------------------------------");
        log.info("User by name {}", this.userRepository.findByUsername("kiran") );
        log.info("-------------------------------------------");
    }





}