package com.classpath.ordersapi.repository;

import com.classpath.ordersapi.model.Customer;
import com.classpath.ordersapi.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findAllByCustomer(Customer customer);
}