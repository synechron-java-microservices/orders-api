package com.classpath.ordersapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class OrderLineItem {

    @Id
    @GeneratedValue(strategy = AUTO)
    private int id;

    private int qty;

    private double pricePerUnit;

    @ManyToOne
    @JoinColumn(name="order_id", nullable = false)
    @JsonIgnore
    private Order order;

}