package com.classpath.ordersapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name="customers")
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long id;

    private String name;

    private LocalDate dob;

    @OneToMany(mappedBy = "customer", cascade = ALL, fetch = EAGER)
    private Set<Order> orders;

    public void addOrder(Order order){
        if (this.orders == null) {
            this.orders = new HashSet<>();
        }
        this.orders.add(order);
        order.setCustomer(this);
    }

}