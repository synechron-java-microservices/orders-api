package com.classpath.ordersapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name="users")
@Data
@ToString
@Builder
@EqualsAndHashCode(of = {"id", "username"})
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue
    private long id;

    private String username;

    @JsonIgnore
    private String password;

    private String emailAddress;

    @ManyToMany(mappedBy = "users", fetch = EAGER)
    private Set<Role> roles;

    public void addRole(Role role){
        if ( this.roles == null){
            this.roles = new HashSet<>();
        }
        this.roles.add(role);
        if (role.getUsers() == null) {
            role.setUsers(new HashSet<>());
        }
        role.getUsers().add(this);
    }
}