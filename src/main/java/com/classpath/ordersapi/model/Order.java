package com.classpath.ordersapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(of = {"id", "customerName"})
@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long id;

    @Min(value = 25000, message = "Order price should be of min 25K")
    private double price;

    @ManyToOne
    @JoinColumn(name="customer_id", nullable = false)
    @JsonIgnore
    private Customer customer;

    @NotNull(message = "Order date cannot be null")
    @FutureOrPresent
    private LocalDate orderDate;

    @OneToMany(mappedBy = "order", cascade = ALL, fetch = EAGER)
    private Set<OrderLineItem> orderLineItems;

    public void addLineItem(OrderLineItem orderLineItem){
        if (this.orderLineItems == null){
            this.orderLineItems = new HashSet<>();
        }
        this.orderLineItems.add(orderLineItem);
        orderLineItem.setOrder(this);
    }
}