package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.User;
import com.classpath.ordersapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class DomainUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Came inside the loadUserByUsername method :: {}", username );
        User domainUser = this.userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid User id "));
        log.info(" Fetched the domain user from the database:: {} ", domainUser );
        return new DomainUserDetails(domainUser);
    }

    static class DomainUserDetails implements UserDetails {

        private final User domainUser;

        public DomainUserDetails(User domainUser) {
            this.domainUser = domainUser;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return this.domainUser
                    .getRoles()
                    .stream()
                    .map(role -> "ROLE_"+role.getRoleName())
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());

        }

        @Override
        public String getPassword() {
            return this.domainUser.getPassword();
        }

        @Override
        public String getUsername() {
            return this.domainUser.getUsername();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }
}