package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    public Order save(Order order){
        log.info("Saved the order inside the table {} ", order);
        return this.orderRepository.save(order);
    }

    public Set<Order> fetchOrders(){
        return  new HashSet<>(this.orderRepository.findAll());
    }

    public Order findById(long orderId){
        return this.orderRepository.findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Order Id passed"));
    }

    @PreAuthorize("ROLE_ADMIN")
    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}