package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.Customer;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.CustomerRepository;
import com.classpath.ordersapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final OrderRepository orderRepository;

    public Set<Customer> fetchCustomers() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("Logged in user :: {} ",((UserDetails)authentication.getPrincipal()).getUsername());
        return new HashSet<>(this.customerRepository.findAll());
    }

    public Set<Order> fetchOrdersByCustomerId(long id) {
        Optional<Customer> optionalCustomer = this.customerRepository.findById(id);
        if (optionalCustomer.isPresent()){
            return new HashSet<>(this.orderRepository.findAllByCustomer(optionalCustomer.get()));
        }else {
            throw new IllegalArgumentException("Invalid customerId");
        }
    }

    public Customer save(Customer customer) {
        return this.customerRepository.save(customer);
    }

    public Customer fetchCustomerById(long id) {
        return this.customerRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Invalid Customer  id"));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteCustomerById() {
    }
}